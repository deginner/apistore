require('should');
var Sails = require('sails');
var app;

// Global before hook
before(function (done) {
  Sails.lift({}, function(err, sails) {
    if (err)
      return done(err);
    app = sails;
    done(err, sails);
  });
});

// Global after hook
after(function (done) {
  app.lower(done);
});