var request = require('supertest');
var should = require('should');

var REQUIRED_ITEM = "Digicel Panama"; // TODO mock up this data...
var mpath = '/item'

describe('ItemController', function() {
  describe('find()', function() {
    it('Can find default list', function (done) {
      request(sails.hooks.http.app)
      .get(mpath)
      .send()
      .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
      .expect('Content-Type', /json/)
      .expect(200, done);
    });
    it('Find with where query', function (done) {
      var query = {"name":{"contains": REQUIRED_ITEM}};
      var jquery = JSON.stringify(query);
      request(sails.hooks.http.app)
      .get(mpath)
      .query({where:jquery})
      .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(error, result) {
        if (error) {
          done(error);
        }
        result.req.path.should.be.equal('/item?where=' + encodeURIComponent(jquery));
        result.body.should.have.lengthOf(1);
        result.body[0].should.have.property['name'];
        result.body[0]['name'].should.containEql(REQUIRED_ITEM);
        done();
      });
    });
    it('Find with bad where query returns empty results', function (done) {
      var query = {"name":{"contains": REQUIRED_ITEM + " bad"}};
      var jquery = JSON.stringify(query);
      request(sails.hooks.http.app)
      .get(mpath)
      .query({where:jquery})
      .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(error, result) {
        if (error) {
          done(error);
        }
        result.req.path.should.be.equal('/item?where=' + encodeURIComponent(jquery));
        result.body.should.have.lengthOf(0);
        done();
      });
    });
    it('Find with malformed where query returns default results', function (done) {
      var query = {"name":{"contains": REQUIRED_ITEM + " bad"}};
      var jquery = JSON.stringify(query).substring(2, 12); // this will no longer be valid json
      request(sails.hooks.http.app)
      .get(mpath)
      .query({where:jquery})
      .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(error, result) {
        if (error) {
          done(error);
        }
        result.req.path.should.be.equal('/item?where=' + encodeURIComponent(jquery));
        result.body.should.not.have.lengthOf(0); // returns default results
        done();
      });
    });
  });
  describe('create()', function() {
    it('Cannot create an item', function (done) {
      request(sails.hooks.http.app)
      .post(mpath)
      .send()
      .expect(403, done);
    });
  });
  describe('update()', function() {
    it('Cannot update an item ', function (done) {
      var hackstring = "IhaxURstore";
      request(sails.hooks.http.app)
      .put(mpath)
      .send({'name': hackstring})
      .expect(403)
      .end(function(err, res) {
        request(sails.hooks.http.app)
        .get(mpath)
        .query({where:JSON.stringify({"name":hackstring})})
        .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(error, result) {
          if (error) {
            done(error);
          }
          result.body.should.have.lengthOf(0);
          done();
        });
      });
    });
  });
  describe('destroy()', function() {
    it('Cannot delete an item', function (done) {
      request(sails.hooks.http.app)
      .delete(mpath)
      .send({'name': REQUIRED_ITEM})
      .expect(403)
      .end(function(err, res) {
        request(sails.hooks.http.app)
        .get(mpath)
        .query({'name': REQUIRED_ITEM})
        .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(error, result) {
          if (error) {
            done(error);
          }
          result.body.should.have.lengthOf(1);
          done();
        });
      });
    });
  });
});