# Api Store

*(pronounced like a cockney bloke saying “happy store”)*

Api Store is a new type of shopping experience. Taking advantage of the trustless, p2p Bitcoin payment network, goods and services can now be purchased with no signup or prior relationship required. The checkout and payment processes can now be completely automated.

Demo at: https://apistore.co

## Project Status
- [x] ***0.1*** (Estimate: 1-6-15)
 - [x] port from express to sails.js
 - [x] Category
 - [x] Quote
 - [x] Order
 - [x] Outlet
   - [x] Bitrefill
- [ ] ***0.2*** (Estimate: 8-6-15)
 - [x] JS client
 - [ ] Web demo client for apistore.co
- [ ] ***0.3*** (Estimate: 15-6-15)
 - [ ] Professional design for apistore.co
 - [ ] Lamassu UI plugin
 - [ ] BIP-70 used for Buy responses
- [ ] ***0.4*** (Estimate: 1-7-15)
 - [ ] Lamassu UI plugin deployed in Panama
 - [ ] Panama Bill Pay (via mensajero)
   - [ ] Fenosa
   - [ ] CableOnda
   - [ ] Cable & Wireless
   - [ ] Claro
   - [x] Digicel
   - [x] Movistar

## Outlets

The Api Store will integrate merchant outlets that meet modest technical criteria.

On the technical front, outlets are stores or services that accept bitcoin payments and operate an API. If you can be paid in a fully automated fashion by strangers, chances are you can be an Api Store outlet.

The Api Store is an open source project, and anyone is free to use or fork it. The maintainers will not include your outlet if you include illegal goods or services. Note that geoblocking is a planned feature, for those who need to block certain countries from their products/services.

The first outlet will be [Bitrefill](http://bitrefill.com).

## User Interfaces
The Api Store is intended to be white labeled by operators of user interfaces. Target intefaces are applications and devices where people have bitcoin to spend. Candidates:

+ Bitcoin Teller Machines
+ Bitcoin wallets
+ SMS wallets

## Client API
The client API is documented over at [Apiary]()http://docs.apistore.apiary.io/.