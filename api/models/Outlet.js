/**
* Outlet.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  // call the update_items function of any services that support it.
  update_items: function() {
    for (var s in sails.services) {
      if (sails.services[s].hasOwnProperty("update_items")) {
        sails.services[s].update_items();
      }
    }
  },

  attributes: {
    name: {
      type: "string",
      required: true
    },
  }
};
