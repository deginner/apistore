/**
* Paymethod.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    name: {
      type: "string",
      required: true,
      defaultsTo: "bitcoin"
    },
    display: {
      type: "string",
      required: true,
      defaultsTo: "Bitcoin"
    },
    currencies_supported: {
      type: 'array'
    },
    logo: {
      type: "url",
      required: false
    },
    items: {
      collection: "item",
      via: "paymethods"
    }
  }
};

