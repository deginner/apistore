module.exports = function bitrefillHook(sails) {
  return {
    initialize: function(cb) {
      sails.once('hook:paymethods:loaded', function() {
        Category.findOrCreate({"name": "Bill Pay"},
                              {name: "Bill Pay",
                               description: "Pay all sorts of bills. Well, currently only mobile phones.",
                               parent: "",}).exec(function(){})
                               
        Outlet.findOrCreate({"name": "Bitrefill"}, {"name": "Bitrefill"}).exec(function(err, doc) {
          if (err) return cb();
          Item.count({'outlet': doc.id}).exec(function(err, num) {
            if (err) return cb();
            if (num == 0) {
              Outlet.update_items();
              return cb();
            } else {
              return cb();
            }
          });
        });
      });
    }
  };
};