/**
 * QuoteController
 *
 * @description :: Server-side logic for managing quotes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create: function(req, res) {
    var params = req.allParams();
    delete params.id;
    var redirect = params.redirect;
    delete params.redirect;
    var csrfx = params._csrf || "generated";
    delete params._csrf;
    Item.findOne({id: params['item']}).exec(function(err, doc) {
      if (err || doc == undefined) {
        return res.status(400);
      }
      var rawQuote = {item: params['item'], 'csrfx': csrfx};
      for (var i = 0; i < doc['required_fields'].length; i++) {
        if (doc['required_fields'][i] in params) {
          rawQuote[doc['required_fields'][i]] = params[doc['required_fields'][i]];
        }
      }
      BitrefillService.create_quote(rawQuote, function(err, prepQuote) {
        Quote.create(prepQuote).populate('item').exec(function(err, q) {
          sails.log.silly(err);
          sails.log.silly(q);
          if (redirect) {
            return res.redirect("/quote/view/" + q.id + "/" + q.csrfx);
          } else {
            return res.ok(q);
          }
        });
      });
    });
  }
};

