/**
 * OrderController
 *
 * @description :: Server-side logic for managing orders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var request = require('request');

module.exports = {
  create: function(req, res) {
    var params = req.allParams();
    delete params.id;
    var redirect = params.redirect;
    delete params.redirect;
    var csrfx = params._csrf;
    delete params._csrf;
    Quote.findOne({id: params['quote']}).populate("item").exec(function(err, doc) {
      if (err || doc == undefined) {
        return res.status(400);
      }
      var rawOrder = {
        item: params['item'],
        quote: params['quote'],
        option: params['option'],
        'csrfx': csrfx
      };
      BitrefillService.create_order(rawOrder, doc, function(err, prepOrder) {
        Order.create(prepOrder).exec(function(err, o) {
          sails.log.silly(err);
          sails.log.silly(o);
          if (redirect) {
            return res.redirect("/order/view/" + o.id + "/" + o.csrfx);
          } else {
            return res.ok(o);
          }
        });
      });
    });
  },
  find: function(req, res) {
    var params = req.allParams();
    Order.findOne({_id: params.id, csrfx: params.csrfx}, function(err, doc) {
      if (err) {
        res.status(403).send("Invalid request");
      } else if (doc['status'] != 'complete') {
        BitrefillService.check_order(doc, function(err, updoc) {
          updoc.save(function(err, newdoc) {
            res.ok(updoc); 
          });
        })
      } else {
        res.ok(doc);
      }
    })
  }
};

